#!/usr/bin/env python
#
# Copyright (c) 2012, Jacob Burbach <jmburbach@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#	Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer. Redistributions in binary
#   form must reproduce the above copyright notice, this list of conditions and
#   the following disclaimer in the documentation and/or other materials
#   provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
from lib.MultipartPostHandler import MultipartPostHandler
import urllib2, cookielib, os, re


def lmtool_compile_corpus_and_fetch_results(corpus_path, lm_path, dic_path):
	cookies = cookielib.CookieJar()
	opener = urllib2.build_opener(
		urllib2.HTTPCookieProcessor(cookies),
		MultipartPostHandler
	)
	params = {
		"formtype": "simple",
		"corpus": open(corpus_path)
	}

	print "sending corpus..."
	r = opener.open("http://www.speech.cs.cmu.edu/cgi-bin/tools/lmtool/run", params)

	url = r.geturl()
	data = r.read()

	print "parsing result..."
	match = re.search(r"this set is <b>([0-9]*)</b>", data)
	if not match:
		raise Exception("failed to parse id from response")

	print "generated url is %s" % url

	result_id = match.groups()[0]
	lm_url = os.path.join(url, "%s.lm" % result_id)
	dic_url = os.path.join(url, "%s.dic" % result_id)

	print "fetching %s..." % lm_url
	request = urllib2.Request(lm_url)
	response = urllib2.urlopen(request)
	open(lm_path, "w").write(response.read())
	print "saved result to %s..." % lm_path

	print "fetching %s..." % dic_url
	request = urllib2.Request(dic_url)
	response = urllib2.urlopen(request)
	open(dic_path, "w").write(response.read())
	print "saved result to %s..." % dic_path


if __name__ == "__main__":
	import sys
	my_path = os.path.abspath(os.path.dirname(__file__))
	my_corpus = os.path.join(my_path, "data", "corpus.txt") 
	my_lm = os.path.join(my_path, "data", "gst_sphinx_cli.lm")
	my_dic = os.path.join(my_path, "data", "gst_sphinx_cli.dic")
	try:
		lmtool_compile_corpus_and_fetch_results(my_corpus, my_lm, my_dic)
	except Exception, e:
		print e
